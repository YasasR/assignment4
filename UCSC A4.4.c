#include <stdio.h>

int main()
{
    float cel,fa;
    printf("Enter temperature in celsius: ");
    scanf("%f",&cel);
    fa = (cel*9/5 + 32);
    printf("%.2f celsius = %.2f fahrenheit",cel,fa);

    return 0;
}
